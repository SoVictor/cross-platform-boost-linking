#include <iostream>

#include "boost/filesystem.hpp"

using boost::filesystem::directory_entry;
using boost::filesystem::directory_iterator;
using boost::filesystem::path;

int main() {
  path p(".");
  for (directory_entry& t : directory_iterator(p))
      std::cout << t.path() << std::endl;

  return 0;
}
