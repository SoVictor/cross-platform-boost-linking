git clean -fxd

cd third_party\boost
call .\bootstrap.bat
.\b2 --layout=tagged --prefix="build" --with-locale link=static runtime-link=static threading=multi variant=debug,release --build-type=complete stage install
cd ..\..

MKDIR build

cmake -G "Visual Studio 16 2019" -S %cd% -B "%cd%\build" -DCMAKE_BUILD_TYPE=Release
cmake --build "%cd%\build" --target Crossroads
cmake --build "%cd%\build" --target LevelEditor
cmake --build "%cd%\build" --target MapBuilder
