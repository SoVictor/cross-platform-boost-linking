#!/usr/bin/env bash

cd ./third_party/boost
./bootstrap.sh
./b2 --layout=tagged --prefix="build" --with-locale link=static runtime-link=static threading=multi variant=debug,release --build-type=complete stage install
cd ./build/lib
ls -ln
cd ./../..
cd ./../..

mkdir ./build
cd ./build

cmake ./.. -DCMAKE_BUILD_TYPE=Release
cmake --build . --target Crossroads
cmake --build . --target LevelEditor
