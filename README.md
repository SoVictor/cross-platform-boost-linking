# cross-platform boost linking

A minimal project with boost included as a git submodule, build from sources and being linked both under Windows and Linux.

The whole pipeline is implemented in GitLab CI too (note, though, that right now there are no CI runners associated with this repo).

## Before build

1. `cd third_party/boost`
1. run `./bootstrap.bat` (for Windows) or `./bootstrap.sh` (for Linux)
1. run `./b2 --layout=tagged --prefix="build" --with-filesystem link=static runtime-link=static threading=multi variant=debug,release --build-type=complete stage install`
